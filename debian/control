Source: libpano13
Section: libs
Priority: optional
Maintainer: Debian PhotoTools Maintainers <pkg-phototools-devel@lists.alioth.debian.org>
Uploaders:
 Andreas Metzler <ametzler@debian.org>,
Build-Depends: dpkg-dev (>= 1.22.5),
 chrpath,
 cmake,
 debhelper-compat (= 13),
 libjpeg-dev,
 libpng-dev,
 libtiff-dev,
 libtiff-tools,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian-phototools-team/libpano
Vcs-Git: https://salsa.debian.org/debian-phototools-team/libpano.git -b libpano13/unstable
Homepage: http://panotools.sourceforge.net/

Package: libpano13-dev
Section: libdevel
Architecture: any
Depends:
 libpano13-3t64 (= ${binary:Version}),
 ${misc:Depends},
Multi-Arch: same
Description: panorama tools library development files
 This package contains the panoramatools library. It provides basic
 algorithmic tools to generate, edit and transform many kinds of
 panoramic images.
 .
 This package holds static libraries and headers needed by developers
 who wish to use libpano13 in their programs.

Package: libpano13-3t64
Provides: ${t64:Provides}
Replaces: libpano13-3
Breaks: libpano13-3 (<< ${source:Version})
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Suggests:
 libpano13-bin,
Multi-Arch: same
Description: panorama tools library
 This package contains the panoramatools library. It provides basic
 algorithmic tools to generate, edit and transform many kinds of
 panoramic images.

Package: libpano13-bin
Section: graphics
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Conflicts:
 libpano12-bin,
Replaces:
 libpano12-bin,
Multi-Arch: foreign
Description: panorama tools utilities
 This package contains the following tools, all part of the Panorama Tools
 library, and supposed to make it easy to work with panoramic images:
  * panoinfo    - List details about the libpano13 library
  * PTblender   - Colour and brightness correction of panoramas
  * PTcrop      - Crop TIFF images
  * PTinfo      - Display information about a panotools-generated image
  * PTmasker    - Compute stitching masks
  * PTmender    - Replacement for PTStitcher
  * PToptimizer - Wrapper around control point optimization routines
  * PTroller    - Merge several images into a single one
  * PTtiff2psd  - Convert a set of TIFF files into a Photoshop PSD file
  * PTtiffdump  - Compare two TIFF images
  * PTuncrop    - Uncrop TIFF images
